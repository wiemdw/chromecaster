""" Flask Chromecast Server """
#pylint: disable=wrong-import-position,wrong-import-order,no-member
import os
import errno
import pipes
import socket
import subprocess
from BaseHTTPServer import BaseHTTPRequestHandler
from urlparse import urlparse, parse_qs


class MediaHandler(BaseHTTPRequestHandler):
    """Media HTTP server handler"""
    content_type = 'video/webm'

    def transcode_media(self, path, srt_path=None, start=0):
        """Transcode media file. """
        bufsize = 0
        filters = [r'scale=-1:min(ih*1920/iw\,1080)',
                   r'pad=1920:1080:(1920-iw)/2:(1080-ih)/2:black']

        args = ['ffmpeg', '-i', path]

        # subtitles
        if srt_path:
            filters += ['subtitles=%s:charenc=CP1252' % pipes.quote(srt_path)]

        # resume -- before or after -i ??
        if start:
            args += ['-ss', start]

        if filters:
            args += ['-vf', ','.join(filters)]

        # WebM encoding -- TODO - libvpx-vp9?
        args += ['-v', 'error', '-copyts', '-c:v', 'libvpx', '-b:v', '4M',
                 '-crf', '10', '-quality', 'realtime', '-cpu-used', '8',
                 '-c:a', 'libvorbis', '-f', 'webm', '-']

        # MP4 H.264 lossless encoding -- experimental / not working properly
        #args += ['-v', 'error', '-copyts', '-c:v', 'libx264', '-b:v', '4M',
        #         '-crf', '0', '-quality', 'realtime', '-cpu-used', '8',
        #         '-c:a', 'libvorbis', '-f', 'mp4', '-movflags', 'frag_keyframe+empty_moov',
        #          '-']

        transcoder = subprocess.Popen(args, stdout=subprocess.PIPE, bufsize=bufsize)
        return transcoder.stdout

    def do_GET(self):
        """Handling incoming GET request"""
        parsed_url = urlparse(self.path)
        request_args = parse_qs(parsed_url.query)

        # get request parameters
        transcode = next(iter(request_args.get('transcode', [])), False)
        start = next(iter(request_args.get('start', [])), 0)
        srt_path = next(iter(request_args.get('subtitles', [])), None)

        # check if media path exists
        path = os.path.join('/', parsed_url.path)
        if not os.path.exists(path):
            self.send_error(404, "Path '%s' not found." % path)

        # open or transcode media path
        media_fd = None
        if transcode not in [False, 'false', 'False']:
            media_fd = self.transcode_media(path, srt_path=srt_path, start=start)
        else:
            media_fd = file(path)

        try:
            self.send_headers()
            self.send_data(media_fd)
        except socket.error, err:
            if err[0] in (errno.EPIPE, errno.ECONNRESET):
                return
            raise

    def send_headers(self):
        """Send headers"""
        self.protocol_version = "HTTP/1.1"
        self.send_response(200)
        self.send_header("Content-type", self.content_type)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header("Transfer-Encoding", "chunked")
        self.end_headers()

    def send_data(self, media_fd):
        """Stream data"""
        while True:
            line = media_fd.read(1024)
            if not line:
                break
            chunk_size = "%0.2X" % len(line)
            self.wfile.write(chunk_size+"\r\n"+line+"\r\n")

        media_fd.close()
        self.wfile.write("0\r\n\r\n")

    def handle_one_request(self):
        try:
            return BaseHTTPRequestHandler.handle_one_request(self)
        except socket.error, err:
            if err[0] in (errno.EPIPE, errno.ECONNRESET):
                return
            raise

    def finish(self):
        try:
            return BaseHTTPRequestHandler.finish(self)
        except socket.error, err:
            if err[0] in (errno.EPIPE, errno.ECONNRESET):
                return
            raise


class SubtitleHandler(MediaHandler):
    """ Handle HTTP requests for subtitles files """
    content_type = "text/vtt;charset=utf-8"
