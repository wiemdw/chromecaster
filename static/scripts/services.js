"use strict";

angular.module('chromecaster')
    .constant("baseURL", "http://127.0.0.1:5000/") // TODO

    /*** Chromecast Server API Service ***/
    .service('cast_service', ['$resource', "baseURL", function ($resource, baseURL) {
        this.connectDevice = function connectDevice(device) {
            return $resource(baseURL + 'connect/:device', {device: device}).get();
        };
        this.listDevices = function listDevices() {
            return $resource(baseURL + 'devices').query();
        };
        this.getStatus = function getStatus(statusType) {
            return $resource(baseURL + 'status/:statusType', {statusType: statusType}).get();
        };
        this.listMedia = function listMedia(path) {
            return $resource(baseURL + 'media/:path', {path: path}).query();
        };
        this.play = function (media, start, transcode, subtitles) {
            var params = {media: media, transcode: transcode};
            if (start && start > 0) { params.start = start; }
            if (subtitles) { params.subtitles = subtitles; }
            return $resource(baseURL + 'play/:media', params).get();
        };
        this.stop = function () {
            return $resource(baseURL + 'stop').get();
        };
        this.pause = function () {
            return $resource(baseURL + 'pause').get();
        };
        this.resume = function () {
            return $resource(baseURL + 'resume').get();
        };
        this.skip = function (seconds) {
            return $resource(baseURL + 'skip/:seconds', {seconds: seconds}).get();
        };
        this.rewind = function (seconds) {
            return $resource(baseURL + 'rewind/:seconds', {seconds: seconds}).get();
        };
        this.volume = function (delta) {
            return $resource(baseURL + 'volume/:delta', {delta: delta}).get();
        };
        this.subtitles = function (state) {
            var state_url = state ? 'enable/1' : 'disable';
            return $resource(baseURL + 'subtitle/' + state_url).get();
        };
    }])

    /*** Error / exception Handling ***/
    .factory('httpInterceptor', ["$rootScope", "$q", function ($rootScope, $q) {
        return {
            responseError: function (response) {
                $rootScope.errorMessage = JSON.stringify(response.data);
                return $q.reject(response);
            }
        };
    }])

   /*** Media Tree Builder ***/
   .factory('tree_builder', [function() {
      var treeBuilder = {};

      treeBuilder.buildTree = function (parts, data, treeNode) {
           parts = parts.filter(function(n){ return n != ''; });
           if(parts.length === 0) { return; }

           var current_node = treeNode;
           for(var i = 0 ; i < parts.length; i++) {

               var found = false;
               for(var j = 0; j < current_node.length; j++) {
                   if (current_node[j].name == parts[i]) {
                       current_node = current_node[j].children;
                       found = true;
                       break;
                   }
               }
               if(!found) {
                   current_node.push({name: parts[i], children: []});
                   if(i == parts.length - 1) {
                       current_node = current_node[current_node.length-1];
                   } else {
                       current_node = current_node[current_node.length-1].children;
                   }
               }
           }
           $.extend( true, current_node, data );
      }

      /* build grid tree from media list */
      treeBuilder.getTree = function (media_list) {
          var media_tree = [];
          for(var i = 0 ; i< media_list.length; i++)
          {
              var dirpaths = media_list[i].filepath.split('/');
              media_list[i].name = dirpaths[dirpaths.length-1];
              treeBuilder.buildTree(dirpaths, media_list[i], media_tree);
          }
          return media_tree;
      };

      return treeBuilder;
   }])
;
