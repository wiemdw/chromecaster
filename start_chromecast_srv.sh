#!/bin/bash
export FLASK_DEBUG=1
source venv/bin/activate
gunicorn --log-level debug --threads 8 -b 0.0.0.0:5000 chrome_server:app
