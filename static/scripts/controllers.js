"use strict";

/***
 Device Controller
***/
angular.module('chromecaster')
  .controller('DeviceController', ['cast_service',
  function (cast_service) {
      var vm = this;

      vm.connectDevice = function (device) {
          console.log("connecting to chromecast '" + device + "'");
          cast_service.connectDevice(device).$promise.then(function success(response) {
              vm.device = device;
          });
      };
      vm.listDevices = function () {
          cast_service.listDevices().$promise.then(function success(response) {
              vm.devices = response;
          });
      };
  }]);

/***
 Media controller
***/
angular.module('chromecaster')
  .controller('CastController', ['$scope', '$interval', '$filter', 'cast_service', 'tree_builder',
  function ($scope, $interval, $filter, cast_service, tree_builder) {
      var vm = this;
      vm.active_ctrl = '';
      vm.layout = 'table';

      /***
       Media queue settings and functions
      ***/
      vm.media_queue = []; // list of queue items
      vm.media_finished = []; // list of finished items
      vm.dragged = false; // flag to distinguish between click and drag

      vm.drag_start = function() {
        vm.dragged = true;
      };
      vm.remove_from_queue = function(media) {
          vm.media_queue = vm.media_queue.filter(function(item) { return item.name !== media.name;});
      };
      vm.load_from_queue = function(media) {
          vm.remove_from_queue(media);
          vm.load(media);
      };
      vm.previous_queue_item = function() {
          // push current media to the front of the queue
          vm.media_queue.unshift(vm.media);
          // load the last item from media_finished
          vm.load(vm.media_finished.pop());
      };
      vm.next_queue_item = function() {
          // move current media to finished list
          if (vm.media) { vm.media_finished.push(vm.media); }
          // load the first item from the media_queue
          vm.load_from_queue(vm.media_queue[0]);
      };

      /***
       Media tree settings
      ***/
      var tree;

      $scope.my_tree = tree = {};
      $scope.col_defs = [
          {
            field: 'duration',
            displayName: 'Duration',
            filterable: true
          }
      ];
      $scope.expanding_property = {
          field: "name",
          displayName: "Title",
          sortable: true,
          filterable: true,
          cellTemplate: "<a href='#media'>{{row.branch[expandingProperty.field]}}</a>"
      };

      /* set progress: function updates 'elapsed' counter and resyncs the status and
         current_time from the Chromecast media status for every percentage increase */
      function setProgress() {
          var duration = $filter('timeToSeconds')(vm.media.duration); // calculate once instead of every tick
          vm.media.progress_pct = Math.round(vm.elapsed/duration*100);

          if (vm.elapsed % 10 == 0) {
              // sync the elapsed timer with the current_time from chromecast
              cast_service.getStatus('media')
                .$promise.then(function success(state) {
                    if(state.player_state !== 'IDLE') {
                      var diff = vm.elapsed - state.current_time;
                      console.log("correcting elapsed time by "+diff+" seconds");
                      vm.elapsed = Math.round(state.current_time);
                    }
                });
          }

          if (vm.elapsed > duration) {
              // check if playback is finished
              cast_service.getStatus('media')
                .$promise.then(function success(state) {
                    if (state.player_state === 'IDLE') {
                        vm.state = 'finished';
                        $interval.cancel(vm.interval);

                        if (vm.media_queue.length > 0) {
                          console.log("starting next file"+JSON.stringify(vm.media_queue[0]));
                          vm.media_finished.push(vm.media);
                          vm.media = vm.media_queue.shift();
                          vm.play(vm.media);
                        }
                      }
                    }
                );
          }
          vm.elapsed++;
      }

      /*****
       Media loader
      *****/
      vm.path = '';  // path entered by the user

      vm.mediaPath = function(keyEvent) {
          if (keyEvent.which === 13) {
              vm.listMedia(vm.path);
          }
      };
      vm.load = function(media) {
          // disable loading media when dragging items
          if(vm.dragged) { vm.dragged = false; return; }

          // stop the current playback if a new media is loaded
          if (vm.media) {
              vm.active_ctrl = '';
              vm.stop();
              vm.state = '';
              vm.elapsed = 0;
          }
          vm.media = media;
      };
      vm.listMedia = function (path) {
          if (path.charAt(0) == "/") { path = path.substr(1); }
          cast_service.listMedia(path)
            .$promise.then(function success(response) {
                vm.media_list = response;
                // convert media list to tree view
                vm.media_tree = tree_builder.getTree(vm.media_list);
            });
      };

      /*****
       Media controls
      *****/
      vm.play = function (media) {
          // strip leading slash to form url path
          var filepath;
          if (media.filepath.charAt(0) == "/") {
              filepath = media.filepath.substr(1);
          } else {
              filepath = media.filepath;
          }

          // set subtitles
          var subtitles = null;
          if (media.subtitles_on && media.subtitles) {
              subtitles = media.subtitles;
          }

          console.log("playing: "+filepath);
          // TODO - start param
          cast_service.play(filepath, 0, vm.media.transcode, subtitles)
            .$promise.then(function success(response) {
                vm.state = 'playing';
                vm.media = media;
                vm.elapsed = 0;
                vm.interval = $interval(setProgress, 1000);
          });
      };
      vm.stop = function () {
          cast_service.stop().$promise.then(function success(response) {
              vm.state = 'stopped';
              vm.elapsed = 0;
              $interval.cancel(vm.interval);
          });
      };
      vm.pause = function () {
          cast_service.pause().$promise.then(function success(response) {
              vm.state = 'paused';
              $interval.cancel(vm.interval);
          });
      };
      vm.resume = function () {
          cast_service.resume().$promise.then(function success(response) {
              vm.state = 'playing';
              vm.interval = $interval(setProgress, 1000);
          });
      };
      vm.skip = function (seconds) {
          cast_service.skip(seconds).$promise.then(function success(response) {
              vm.state = 'playing';
              vm.elapsed += seconds;
          });
      };
      vm.rewind = function (seconds) {
          cast_service.rewind(seconds).$promise.then(function success(response) {
              vm.state = 'playing';
              vm.elapsed -= seconds;
          });
      };
      vm.volume = function (delta) {
          cast_service.volume(delta).$promise.then(function success(response) {
          });
      };
      vm.subtitles = function (state) {
          cast_service.subtitles(state).$promise.then(function success(response) {
          });
      };
}]);
