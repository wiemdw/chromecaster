'use strict';

angular.module('chromecaster', ['ui.router', 'ngResource', 'treeGrid', 'smart-table', 'ngDragDrop'])
    /*** URL Router ***/
    .config(function ($stateProvider, $urlRouterProvider, $resourceProvider) {
        $stateProvider
            // route for the home page
            .state('app', {
                url: '/',
                views: {
                    'chromecast': {
                        templateUrl: 'static/views/chromecast.html',
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
        $resourceProvider.defaults.stripTrailingSlashes = false;
    })

    /*** HTTP interceptor ***/
    .config(['$httpProvider',function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    }])

    /*** Filters ***/
    .filter('secondsToDateTime', [function () {
        return function (seconds) {
            return new Date(1970, 0, 1).setSeconds(seconds);
        };
    }])
    .filter('returnInt', function () {
        return function (number) {
            return parseInt(number, 10);
        };
    })
    .filter('timeToSeconds', ['returnIntFilter',
        function (returnIntFilter) {
            return function (time) {
                return time.split(':')
                    .reverse()
                    .map(returnIntFilter)
                    .reduce(function (pUnit, cUnit, index) {
                        return pUnit + cUnit * Math.pow(60, index);
                    });
            };
    }])

    /*** Directives ***/
    .directive('resizer', function($document) {
    	return function($scope, $element, $attrs) {
    		$element.on('mousedown', function(event) {
    			event.preventDefault();
    			$document.on('mousemove', mousemove);
    			$document.on('mouseup', mouseup);
    		});
    		function mousemove(event) {
    			if ($attrs.resizer == 'vertical') {
    				// Handle vertical resizer
    				var x = event.pageX;
    				if ($attrs.resizerMax && x > $attrs.resizerMax) {
    					x = parseInt($attrs.resizerMax);
    				}
    				$element.css({left: x + 'px'});
    				$($attrs.resizerLeft).css({width: x + 'px'});
    				$($attrs.resizerRight).css({left: (x + parseInt($attrs.resizerWidth)) + 'px'});
    			} else {
    				// Handle horizontal resizer
    				var y = window.innerHeight - event.pageY;
    				$element.css({bottom: y + 'px'});
    				$($attrs.resizerTop).css({bottom: (y + parseInt($attrs.resizerHeight)) + 'px'});
    				$($attrs.resizerBottom).css({height: y + 'px'});
    			}
    		}
    		function mouseup() {
    			$document.unbind('mousemove', mousemove);
    			$document.unbind('mouseup', mouseup);
    		}
    	};
    })
;
