""" Flask Chromecast Server """
#pylint: disable=wrong-import-position,wrong-import-order,no-member
import os
import json
import time
import pipes
import socket
import urllib
import subprocess
import mimetypes
from functools import wraps
from threading import Thread
from BaseHTTPServer import HTTPServer
from uuid import UUID
from flask import Flask, abort, request, make_response
from http_handlers import MediaHandler, SubtitleHandler
import pychromecast

#pychromecast.IGNORE_CEC.append('*')  # Ignore CEC on all devices

app = Flask(__name__)

MEDIA_EXTENSIONS = ['.mkv', '.avi', '.mp4', '.wmv', '.mpg', '.mpeg']
SUBTITLE_EXTENSIONS = ['.srt', '.sub']
SUBTITLE_DIRECTORIES = ['Subs', 'subs', 'Subtitles', 'subtitles']
FFPROBE_DURATION_CMD = "ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 %s"
DEFAULT_CONTENT_TYPE = 'video/webm'
MEDIA_PORT = 5001
SUBTITLE_PORT = 5002

# Support for negative floats in url path
from werkzeug.routing import FloatConverter as BaseFloatConverter

class FloatConverter(BaseFloatConverter):
    regex = r'-?\d+(\.\d+)?'

app.url_map.converters['float'] = FloatConverter
app.url_map.strict_slashes = False


# pychromecast objects contains non-serializable UUIDs
# https://arthurpemberton.com/2015/04/fixing-uuid-is-not-json-serializable
JSONEncoder_olddefault = json.JSONEncoder.default
def JSONEncoder_newdefault(self, o):
    if isinstance(o, UUID):
        return str(o)
    return JSONEncoder_olddefault(self, o)
json.JSONEncoder.default = JSONEncoder_newdefault


@app.errorhandler(Exception)
def handle_exception(exc):
    app.logger.error('Unhandled Exception: %s', (exc))
    return make_response(str(exc), 500)


def check_cast_connection(func):
    """Check if the required Chromecast connection exists"""
    @wraps(func)
    def check_cast(*args, **kwargs):
        if not hasattr(app, 'cast') or not app.cast:
            return make_response("No Chromecast connection found", 500)
        return func(*args, **kwargs)

    return check_cast


def response(resp):
    """Generic response handler"""
    try:
        return app.response_class(
            response=json.dumps(resp),
            status=200,
            mimetype='application/json'
        )
    except Exception as ex:
        app.logger.error(ex)
        abort(500)


def get_files(directory, suffix=None):
    """Get file paths from directory """
    for name in os.listdir(directory):
        full_path = os.path.join(directory, name)
        if os.path.isdir(full_path):
            for entry in get_files(full_path, suffix=suffix):
                yield entry
        elif os.path.isfile(full_path):
            if isinstance(suffix, basestring) and full_path.endswith(suffix):
                yield full_path
            elif isinstance(suffix, list) and any([full_path.endswith(suff) for suff in suffix]):
                yield full_path
            elif not suffix:
                yield full_path


def get_media_duration(filepath):
    """Get duration of media file path """
    cmd = FFPROBE_DURATION_CMD % pipes.quote(filepath)
    duration = 0.0
    try:
        duration = float(subprocess.check_output(cmd, shell=True))
        duration = time.strftime('%H:%M:%S', time.gmtime(duration))
    except Exception as ex:
        app.logger.error("failed to get duration for %s: %s", filepath, ex)
    return duration


def get_local_ip(chromecast_ip=None):
    """Get local IP to serve media files """
    target = chromecast_ip or '8.8.8.8'
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    try:
        s.connect((target, 80))
        ip = s.getsockname()[0]
    except socket.error:
        # Fallback to hostname, hoping for proper DNS resolving..
        ip = socket.gethostname()
    except Exception as ex:
        raise Exception("Failed to define local IP: %s" % ex)
    return ip


def find_subtitles(filepath):
    """Try to find subtitles for a given media file path """
    # TODO - return multiple matches?
    file_no_ext = os.path.splitext(filepath)[0]
    dirpath = os.path.dirname(filepath)

    # same directory, same filename, subtitle extension
    for sub_ext in SUBTITLE_EXTENSIONS:
        subtitle_full_path = file_no_ext + sub_ext
        if os.path.isfile(subtitle_full_path):
            return subtitle_full_path

    # subtitle in a 'subtitle' subdirectory
    filename_no_ext = os.path.basename(file_no_ext)
    dirpath = os.path.dirname(filepath)

    for sub_dir in SUBTITLE_DIRECTORIES:
        sub_path = os.path.join(dirpath, sub_dir)
        if not os.path.isdir(sub_path):
            continue  # subtitle subdirectory does not exit

        for sub_ext in SUBTITLE_EXTENSIONS:
            subtitle_full_path = os.path.join(sub_path, filename_no_ext) + sub_ext
            if os.path.isfile(subtitle_full_path):
                return subtitle_full_path

    # giving up
    return None


@app.route('/devices')
def list_devices():
    """List Chromecast devices on the network """
    chromecasts = pychromecast.get_chromecasts()
    return response([cc.device.friendly_name for cc in chromecasts])


@app.route('/connect/<path:device>')
def connect(device):
    """Connect to Chromecast device """
    chromecasts = pychromecast.get_chromecasts()
    app.logger.info("connecting to device '%s'", device)
    cast = [cc for cc in chromecasts if cc.device.friendly_name == device][0]
    setattr(app, 'cast', cast)

    if not app.cast:
        return make_response("Failed to connect to %s" % device, 500)

    app.cast.wait()
    app.cast.volume_up(1.0)  # reset volume
    return response({'status': app.cast.device})


# Status calls
@app.route('/status/cast/')
@check_cast_connection
def cast_status():
    """Cast status."""
    return response(app.cast.status)


@app.route('/status/device/')
@check_cast_connection
def device_status():
    """Device status."""
    return response(app.cast.device)


@app.route('/status/media/')
@check_cast_connection
def media_status():
    """Media status."""
    app.cast.media_controller.update_status()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/media/<path:rootpath>')
def list_media(rootpath):
    """List all media files in the given path, including subtitles,
    duration and filename."""
    app.logger.info("listing files in %s", rootpath)
    media_list = list()
    media_path = os.path.join('/', rootpath)

    # handle errors
    if not os.path.exists(media_path):
        return make_response("Path '%s' not found." % media_path, 404)
    if not os.path.isdir(media_path):
        return make_response("Path '%s' is not a directory." % media_path, 500)

    for media_file in get_files(media_path, MEDIA_EXTENSIONS):
        subtitles = find_subtitles(media_file)
        media = {'name': media_file.replace(rootpath, '').strip("/"),
                 'filepath': media_file,
                 'subtitles': subtitles,
                 'subtitles_on': True if subtitles else False,
                 'transcode': True,
                 'duration': get_media_duration(media_file)}
        media_list.append(media)

    media_list = sorted(media_list, key=lambda k: k['name'])
    return response(media_list)


@app.route('/play/<path:media>')
@check_cast_connection
def play(media):
    """Play media"""
    # get server IP
    server_ip = get_local_ip(app.cast.host)
    app.logger.info("server -> chromecast: %s -> %s", server_ip, app.cast.host)

    # build media URL
    media_url = 'http://' + server_ip + ':' + MEDIA_PORT + '/' + media
    query = list()

    # TODO try to define if the current file requires transcoding.
    transcode = request.args.get('transcode')

    # check for subtitles
    subtitles_url = None
    full_path = os.path.join('/', media)
    srt_path = request.args.get('subtitles') or os.path.splitext(full_path)[0] + '.srt'
    if os.path.isfile(srt_path):
        if transcode:
            query.append('subtitles=%s' % urllib.quote(srt_path))
        else:
            subserver = HTTPServer(('', SUBTITLE_PORT), SubtitleHandler)
            Thread(target=subserver.handle_request).start()
            subtitles_url = 'http://' + server_ip + ':' + SUBTITLE_PORT + srt_path

    # check mimetype
    content_type = mimetypes.guess_type(full_path)[0] or DEFAULT_CONTENT_TYPE

    # forward any other parameters (start, transcode, ...)
    query.extend(['%s=%s' % (key, request.args.get(key)) for key in request.args if key != 'subtitles'])
    media_url += '?%s' % '&'.join(query)
    app.logger.info('media URL %s', media_url)

    # Launch HTTP server to serve the media
    server = HTTPServer(('', MEDIA_PORT), MediaHandler)
    Thread(target=server.handle_request).start()

    # Call Chromecast to play media
    app.cast.media_controller.play_media(media_url,
                                         content_type=content_type,
                                         subtitles=subtitles_url)
    app.cast.media_controller.block_until_active()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/pause')
@check_cast_connection
def pause():
    """Pause media playback"""
    app.logger.info('pausing %s',
                    app.cast.media_controller.status.content_id)
    app.cast.media_controller.pause()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/resume')
@check_cast_connection
def resume():
    """Resume media playback"""
    app.logger.info('resuming %s',
                    app.cast.media_controller.status.content_id)
    app.cast.media_controller.play()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/stop')
@check_cast_connection
def stop():
    """Stop media playback"""
    app.logger.info('stopping %s',
                    app.cast.media_controller.status.content_id)
    app.cast.media_controller.stop()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/skip/<int:seconds>')
@check_cast_connection
def skip(seconds):
    """Skip media playback forward"""
    app.cast.media_controller.update_status()
    app.logger.info('skipping %s for %i seconds',
                    app.cast.media_controller.status.content_id, seconds)
    current_position = app.cast.media_controller.status.current_time
    app.cast.media_controller.seek(current_position + seconds)
    return response(app.cast.media_controller.status.__dict__)


@app.route('/rewind/<int:seconds>')
@check_cast_connection
def rewind(seconds):
    """Skip media playback backward"""
    app.cast.media_controller.update_status()
    app.logger.info('rewinding %s for %i seconds',
                    app.cast.media_controller.status.content_id, seconds)
    current_position = app.cast.media_controller.status.current_time
    app.cast.media_controller.seek(current_position - seconds)
    return response(app.cast.media_controller.status.__dict__)


@app.route('/volume/<float:delta>')
@check_cast_connection
def volume(delta):
    """Control media playback volume"""
    app.logger.info('setting volume for %s to %i',
                    app.cast.media_controller.status.content_id, delta)
    if delta < 0:
        app.cast.volume_down(delta=abs(delta))
    else:
        app.cast.volume_up(delta=delta)
    return response(app.cast.media_controller.status.__dict__)


@app.route('/subtitle/enable/<int:trackid>')
@check_cast_connection
def subtitle_enable(trackid=1):
    """Enable media subtitles"""
    app.logger.info('enabling subtitles for %s',
                    app.cast.media_controller.status.content_id)
    app.cast.media_controller.enable_subtitle(trackid)
    return response(app.cast.media_controller.status.__dict__)


@app.route('/subtitle/disable')
@check_cast_connection
def subtitle_disable():
    """Disable media subtitles"""
    app.logger.info('disabling subtitles for %s',
                    app.cast.media_controller.status.content_id)
    app.cast.media_controller.disable_subtitle()
    return response(app.cast.media_controller.status.__dict__)


@app.route('/site')
def site():
    """Serve Chromecaster site"""
    return make_response(open('templates/index.html').read())
